variable "domain_name" {
  description = "(Required) A domain name for which the certificate should be issued"
  type        = string
}
